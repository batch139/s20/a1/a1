console.log(`Hello World`);

//JSON Objects
/*{
	"city": "Quezon City",
	"province": "Metro Manila",
	"country": "Philippines"
}

// JSON Arrays
"cities": [
	{"city": "Quezon City", "province": "Metro Manila",
	"country": "Philippines"},
	{
		"city": "Manila City",
		"province": "Metro Manila",
		"country": "Philippines"
	},
	{
		"city": "Makati City",
		"province": "Metro Manila",
		"country": "Philippines"
	}
]
*/

//Converting JS Data into Stringified JSON

let batchesArr = [
	{
		batchName: 'Batch 139',
		firstName: 'Imman'
	},	
	{
		batchName: 'Batch 139',
		firstName: 'Marge'
	}
]

console.log(batchesArr);

console.log( JSON.stringify(batchesArr) );

let data = JSON.stringify({
	name: 'Calvin',
	age: 18,
	address: {
		city: 'Manila',
		country: 'Philippines'
	}
})

console.log(data);

let batchesJSON = `[
	{
		"batchName": "Batch139",
		"firstName": "Calvin"
	},
	{
		"batchName": "Batch139",
		"firstName": "Marge"
	}
]`

console.log(batchesJSON);

console.log( JSON.parse(batchesJSON) );



















